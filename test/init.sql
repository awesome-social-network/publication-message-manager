CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE SCHEMA "publicationmanager";

CREATE TABLE "publicationmanager"."People" (
    "id" uuid DEFAULT uuid_generate_v4() NOT NULL,
    "firstName" character varying NOT NULL,
    "lastName" character varying NOT NULL,
    "email" character varying NOT NULL,
    "city" character varying NOT NULL,
    "createdAt" timestamp DEFAULT now(),
    "updatedAt" timestamp,
    "username" character varying,
    "birthday" character varying,
    CONSTRAINT "person_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE TABLE "publicationmanager"."publication" (
    "id" uuid DEFAULT uuid_generate_v4() NOT NULL,
    "peopleId" uuid NOT NULL,
    "attachmentId" uuid,
    "content" character varying(1000),
    "updatedAt" timestamp,
    "createdAt" timestamp DEFAULT now(),
    CONSTRAINT "publication_id" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE TABLE "publicationmanager"."SharedPublication" (
    "publicationId" uuid NOT NULL,
    "peopleId" uuid NOT NULL,
    "createdAt" timestamp NOT NULL,
    "updatedAt" timestamp NOT NULL,
    CONSTRAINT "SharedPublication_peopleId_fkey" FOREIGN KEY ("peopleId") REFERENCES "publicationmanager"."People"(id) NOT DEFERRABLE,
    CONSTRAINT "SharedPublication_publicationId_fkey" FOREIGN KEY ("publicationId") REFERENCES "publicationmanager".publication(id) NOT DEFERRABLE
) WITH (oids = false);

commit;
