var expect  = require('chai').expect;
var assert  = require('chai').assert;
var request = require('request');
const rp = require('request-promise');

it('Main page content', function(done) {
    this.timeout(15000);

    getAdminToken()
        .then(tokenResponse => {
            var tokenParsedResponse = JSON.parse(tokenResponse);
            rp.get({
                method: 'GET',
                uri: 'http://' + process.env.PUBLICATION_MANAGER_IP + ':8081/publications',
                headers: {
                    'Authorization': 'Bearer ' + tokenParsedResponse.access_token
                },
                resolveWithFullResponse: true
            })
                .then((result) => {
                    expect(result.statusCode).to.equal(200);
                    expect(JSON.parse(result.body).length).to.equal(0);
                    done()
                })
                .catch((error) => {
                    assert.fail(error)
                })
        })
        .catch(error => {
            assert.fail(error)
        })
});

// May be useful if you want to retrieve information from keycloak
async function getAdminToken() {
    return new Promise((resolve,reject) => {
        rp.post('http://keycloak.vitisb.fr/auth/realms/keycloak-user-manager/protocol/openid-connect/token',{
            form:{
                client_id: 'vue-test-app',
                username: process.env.KEYCLOAK_USER,
                password: process.env.KEYCLOAK_PWD,
                grant_type: 'password'
            }
        })
            .then((result) => resolve(result))
            .catch((error) => reject(error))
    })
}