require('dotenv').config({path:'../.env'})
module.exports = {
    express: require('express'),
    session : require('express-session'),
    swaggerUi : require('swagger-ui-express'),
	keycloak: require('keycloak-connect')
}