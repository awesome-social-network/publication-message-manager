const publicationRepository = require('../repository/publications-repository');
const sharedPublicationRepository = require('../repository/shared-publication-repository');
const err = require('../model/errors');

module.exports = {

    getPublications: function() {
        return new Promise((resolve, reject) => {
            return publicationRepository.getPublications()
                .then((result) => {
                    this.addSharedPublicationToResult(result)
                        .then((json) => {
                            resolve(json)
                        })
                        .catch((error) => {
                            reject(error);
                        })
                    })
                .catch((error) => {
                    reject(error);
                })
        })
    },
    addSharedPublicationToResult: function(result) {
        return new Promise((resolve, reject) => {
            let json = JSON.parse(result);
            let promiseArray = [];
            for (let i = 0; i < json.length; i++){
                var promise = sharedPublicationRepository.getPublicationSharedUser(json[i].peopleId, json[i].id)
                    .then((result) => {
                        json[i].sharedPeople = JSON.parse(result);
                    })
                    .catch((error) => {
                        reject(error);
                    });
                promiseArray.push(promise);
            }
            if(json.length === 0) {
                resolve(result)
            } else {
                Promise.all(promiseArray)
                    .then((result) => {
                        resolve(JSON.stringify(json, null, 4))
                    })
            }
        })
    },
    getUserPublications: function(peopleId) {
        return new Promise((resolve, reject) => {
            return publicationRepository.getUserPublications(peopleId)
                .then((result) => {
                    this.addSharedPublicationToResult(result)
                        .then((json) => {
                            resolve(json)
                        })
                        .catch((error) => {
                            reject(error);
                        })
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    deletePublication: function(publicationId) {
        return new Promise((resolve, reject) => {
            return publicationRepository.deletePublication(publicationId)
                .then(() => resolve())
                .catch((error) => reject(error))
        })
    },
    createPublication: function (userId, data) {
        return new Promise((resolve, reject) => {
            if (data.content === undefined) {
                reject(new err.Error(err.errors.INVALID_REQUEST, "Invalid Body"));
                return
            }
            return publicationRepository.createPublication(data.content, userId,data.attachmentId)
                .then(() => resolve())
                .catch((error) => reject(error))
        })
    },
    updatePublication: function (publicationId, data) {
        return new Promise((resolve, reject) => {
            if (data.content === undefined) {
                reject(new err.Error(err.errors.INVALID_REQUEST, "Invalid Body"));
                return
            }
            return publicationRepository.updatePublication(publicationId, data.content)
                .then(() => resolve())
                .catch((error) => reject(error))
        })
    },
    createSharedPublication: function (ownerId, peopleId, data) {
        return new Promise((resolve, reject) => {
            if (data.content === undefined) {
                reject(new err.Error(err.errors.INVALID_REQUEST, "Invalid Body"));
                return
            }
            return publicationRepository.createPublication(data.content, ownerId)
                .then((result) => {
                    sharedPublicationRepository.createSharedPublications(ownerId, result.id)
                        .then(() => {
                            sharedPublicationRepository.createSharedPublications(peopleId, result.id)
                                .then(() => {
                                    resolve()
                                })
                                .catch((error) => reject(error))
                        })
                        .catch((error) => reject(error))
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
}