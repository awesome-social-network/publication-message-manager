const publicationService = require('../service/publications-service');
const err = require('../model/errors');
const jwt = require('jsonwebtoken');

module.exports = {
    getPublications: function(req, res) {
        publicationService.getPublications()
            .then((result) => {
                res.status(200).send(result)
            })
            .catch((error) => {
                if(error instanceof err.Error) {
                    if(error.code === err.errors.INVALID_REQUEST) {
                        res.status(400).send(error)
                    } else if (error.code === err.errors.DATA_BASE_ERROR) {
                        res.status(500).send(error)
                    } else {
                        res.status(500).send(error)
                    }
                } else {
                    res.status(500).send(error)
                }
            })
    },
    getCurrentUserPublications: function(req, res) {
        let result = this.getUserIdFromAuthorizationToken(req);
        if(result instanceof err.Error) {
            res.status(400).send(result);
        }

        publicationService.getUserPublications(result)
            .then((result) => {
                res.status(200).send(result)
            })
            .catch((error) => {
                if(error instanceof err.Error) {
                    if(error.code === err.errors.INVALID_REQUEST) {
                        res.status(400).send(error)
                    } else if (error.code === err.errors.DATA_BASE_ERROR) {
                        res.status(500).send(error)
                    } else {
                        res.status(500).send(error)
                    }
                } else {
                    res.status(500).send(error)
                }
            })
    },
    getUserPublications: function(req, res) {
        publicationService.getUserPublications(req.params.id)
            .then((result) => {
                res.status(200).send(result)
            })
            .catch((error) => {
                if(error instanceof err.Error) {
                    if(error.code === err.errors.INVALID_REQUEST) {
                        res.status(400).send(error)
                    } else if (error.code === err.errors.DATA_BASE_ERROR) {
                        res.status(500).send(error)
                    } else {
                        res.status(500).send(error)
                    }
                } else {
                    res.status(500).send(error)
                }
            })
    },
    deletePublication: function(req, res) {
        publicationService.deletePublication(req.params.id)
            .then((result) => {
                res.status(204).send(result)
            })
            .catch((error) => {
                if(error instanceof err.Error) {
                    if(error.code === err.errors.INVALID_REQUEST) {
                        res.status(400).send(error)
                    } else if (error.code === err.errors.DATA_BASE_ERROR) {
                        res.status(500).send(error)
                    } else {
                        res.status(500).send(error)
                    }
                } else {
                    res.status(500).send(error)
                }
            })
    },
    createPublication: function(req, res) {
        let result = this.getUserIdFromAuthorizationToken(req);
        if(result instanceof err.Error) {
            res.status(400).send(result);
        }

        publicationService.createPublication(result, req.body)
            .then((result) => {
                res.status(201).send(result)
            })
            .catch((error) => {
                if(error instanceof err.Error) {
                    if(error.code === err.errors.INVALID_REQUEST) {
                        res.status(400).send(error)
                    } else if (error.code === err.errors.DATA_BASE_ERROR) {
                        res.status(500).send(error)
                    } else {
                        res.status(500).send(error)
                    }
                } else {
                    res.status(500).send(error)
                }
            })
    },
    updatePublication: function (req, res) {
        publicationService.updatePublication(req.params.id, req.body)
            .then((result) => {
                res.status(204).send(result)
            })
            .catch((error) => {
                if(error instanceof err.Error) {
                    if(error.code === err.errors.INVALID_REQUEST) {
                        res.status(400).send(error)
                    } else if (error.code === err.errors.DATA_BASE_ERROR) {
                        res.status(500).send(error)
                    } else {
                        res.status(500).send(error)
                    }
                } else {
                    res.status(500).send(error)
                }
            })
    },
    createSharedPublication: function (req, res) {
        let result = this.getUserIdFromAuthorizationToken(req);
        if(result instanceof err.Error) {
            res.status(400).send(result);
        }

        publicationService.createSharedPublication(result, req.params.id, req.body)
            .then((result) => {
                res.status(204).send(result)
            })
            .catch((error) => {
                if(error instanceof err.Error) {
                    if(error.code === err.errors.INVALID_REQUEST) {
                        res.status(400).send(error)
                    } else if (error.code === err.errors.DATA_BASE_ERROR) {
                        res.status(500).send(error)
                    } else {
                        res.status(500).send(error)
                    }
                } else {
                    res.status(500).send(error)
                }
            })
    },
    getUserIdFromAuthorizationToken: function (req) {
        let tokenUserConverted = req.get('Authorization').substr(7)
        if(tokenUserConverted === undefined)
            return new err.Error(err.errors.INVALID_TOKEN, 'Token invalid');
        let tokenDecoded = jwt.decode(tokenUserConverted)
        if (tokenDecoded['sub']  === undefined) {
            return new err.Error(err.errors.INVALID_TOKEN, 'Token invalid');
        }
        return tokenDecoded['sub']
    }
}