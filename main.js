//Load express module with `require` directive
const express = require('express');
const publicationController = require('./controller/publications-controller');
const cors = require("cors");
const global = require('./config/config')
const keycloakConfig = require('./config/keycloak')
const memoryStore = new global.session.MemoryStore();
const keycloak = new global.keycloak({ store: memoryStore },keycloakConfig.kcConfig)
const app = express();
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json');
const bodyParser = require('body-parser')

app.use(bodyParser.json())

app.use(global.session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    store: memoryStore
  }));
  app.use(keycloak.middleware());

const corsOptions = {
    origin: 'http://social-network.vitisb.fr',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

if(process.env.NODE_ENV === 'production') {
    app.use(cors(corsOptions))
} else {
    app.use(cors())
}

app.get('/publications',keycloak.protect(), function(req, res) {
    publicationController.getPublications(req, res)
});

app.get('/publications/user',keycloak.protect(), function(req, res) {
    publicationController.getCurrentUserPublications(req, res)
});

app.get('/publications/user/:id',keycloak.protect(), function(req, res) {
    publicationController.getUserPublications(req, res)
});

app.delete('/publications/:id',keycloak.protect(), function(req, res) {
    publicationController.deletePublication(req, res)
});

app.post('/publications',keycloak.protect(), function(req, res) {
    publicationController.createPublication(req, res)
});

app.put('/publications/:id',keycloak.protect(), function(req, res) {
    publicationController.updatePublication(req, res)
});

app.post('/publications/user/:id', keycloak.protect(), function(req, res) {
    publicationController.createSharedPublication(req, res)
});

app.use('/api-docs', swaggerUi.serve,swaggerUi.setup(swaggerDocument));

//Launch listening server on port 8081
app.listen(8081, function () {
    console.log('app listening on port 8081!')
});