const errors = {
    DATA_BASE_ERROR: '1000',
    INVALID_REQUEST: '1001',
    INVALID_TOKEN: '1002'
}

class Error {
    constructor(code, message) {
        this.code = code;
        this.message = message;
    }
}

module.exports = {
    errors,
    Error
}

