const Sequelize = require('sequelize');
const database = require('../model/database').database;

const SharedPublication = database.define('SharedPublication', {
    peopleId: {
        foreignKey: true,
        type: Sequelize.UUID,
    },
    publicationId: {
        foreignKey: true,
        type: Sequelize.UUID,
        allowNull: false
    },
    updatedAt: {
        type: Sequelize.DATE,
    },
    createdAt: {
        type: Sequelize.DATE,
    }
}, {
    // options
});
SharedPublication.removeAttribute('id');
module.exports = SharedPublication;