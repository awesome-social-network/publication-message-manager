const Sequelize = require('sequelize');
const database = require('../model/database').database;

const Publication = database.define('publication', {
    id: {
        primaryKey: true,
        type: Sequelize.UUID
    },
    content: {
        type: Sequelize.STRING,
        allowNull: true
    },
    peopleId: {
        allowNull: false,
        type: Sequelize.UUID
    },
    attachmentId: {
        allowNull: true,
        type: Sequelize.UUID
    },
    updatedAt: {
        type: Sequelize.DATE,
    },
    createdAt: {
        type: Sequelize.DATE,
    }
}, {
    // options
});

module.exports = Publication;