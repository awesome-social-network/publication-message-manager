const SharedPublication = require('../model/shared-publication');
const Sequelize = require('sequelize');
const err = require('../model/errors');

module.exports = {
    createSharedPublications: function(peopleId, publicationId) {
        return new Promise((resolve, reject) => {
            SharedPublication.create({
                publicationId: publicationId,
                peopleId: peopleId,
            })
                .then(function(result) {
                    resolve(result);
                })
                .catch(error => {
                    reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
                })
        })
    },
    getPublicationSharedUser: function(ownerId, publicationId) {
        return new Promise((resolve, reject) => {
            SharedPublication.findAll({
                where: {
                    publicationId: publicationId,
                    peopleId: {
                        [Sequelize.Op.not]: ownerId
                    }
                }
            })
                .then(publications => {
                    resolve(JSON.stringify(publications, null, 4));
                })
                .catch(error => {
                    reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
                })
        })
    }
}