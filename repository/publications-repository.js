const Publication = require('../model/publication');
var err = require('../model/errors');

module.exports = {
    getPublications: function() {
        return new Promise((resolve, reject) => {
            Publication.findAll(
                { order: [ ['updatedAt', 'DESC'] ] })
                .then(publications => {
                    console.log("Publication.findAll().then()")
                    resolve(JSON.stringify(publications, null, 4));
                })
                .catch(error => {
                    console.log("Publication.findAll().error(): " + error)
                    reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
                })
        })
    },
    getUserPublications: function(peopleId) {
        return new Promise((resolve, reject) => {
            Publication.findAll({
                where: {
                    peopleId: peopleId,
                },
                order: [ ['createdAt', 'DESC'] ]
            })
                .then(publications => {
                    console.log("Publication.findAll().then()")
                    resolve(JSON.stringify(publications, null, 4));
                })
                .catch(error => {
                    console.log("Publication.findAll().error(): " + error)
                    reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
                })
        })
    },
    deletePublication: function(id) {
        return new Promise((resolve, reject) => {
            Publication.destroy({
                where: {
                    id: id
                }
            })
                .then(function() {
                    console.log("Publication.destroy().then()")
                    resolve();
                })
                .catch(error => {
                    console.log("Publication.destroy().error()")
                    reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
                })

        })
    },
    createPublication: function(publicationContent, peopleId,attachmentId) {
        return new Promise((resolve, reject) => {
            Publication.create({ content: publicationContent, peopleId: peopleId,attachmentId: attachmentId})
                .then(function(result) {
                    console.log("Publication.create().then() " + JSON.stringify(result));
                    resolve(result);
                })
                .catch(error => {
                    console.log("Publication.create().error() " + error)
                    reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
                })

        })
    },
    updatePublication: function (publicationId, publicationContent) {
        return new Promise((resolve, reject) => {
            Publication.update(
                { content: publicationContent, updatedAt: Date.now()},
                {where: {id : publicationId}}
                )
                .then(function() {
                    console.log("Publication.create().then()")
                    resolve();
                })
                .catch(error => {
                    console.log("Publication.create().error() " + error)
                    reject(new err.Error(err.errors.DATA_BASE_ERROR, error));
                })

        })
    }
}